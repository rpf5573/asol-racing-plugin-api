<?php

class AVS_Post_Type {

    protected AVS_Loader $loader;

    public function __construct(AVS_Loader $loader) {
        $this->loader = $loader;
        $this->loader->add_action('init', $this, 'register_game_post_type');
    }

    public function register_game_post_type() {
        $labels = [
            'name'                     => esc_html__( '게임', 'your-textdomain' ),
            'singular_name'            => esc_html__( 'game', 'your-textdomain' ),
            'add_new'                  => esc_html__( 'Add New', 'your-textdomain' ),
            'add_new_item'             => esc_html__( 'Add New game', 'your-textdomain' ),
            'edit_item'                => esc_html__( 'Edit game', 'your-textdomain' ),
            'new_item'                 => esc_html__( 'New game', 'your-textdomain' ),
            'view_item'                => esc_html__( 'View game', 'your-textdomain' ),
            'view_items'               => esc_html__( 'View 게임', 'your-textdomain' ),
            'search_items'             => esc_html__( 'Search 게임', 'your-textdomain' ),
            'not_found'                => esc_html__( 'No 게임 found.', 'your-textdomain' ),
            'not_found_in_trash'       => esc_html__( 'No 게임 found in Trash.', 'your-textdomain' ),
            'parent_item_colon'        => esc_html__( 'Parent game:', 'your-textdomain' ),
            'all_items'                => esc_html__( 'All 게임', 'your-textdomain' ),
            'archives'                 => esc_html__( 'Game Archives', 'your-textdomain' ),
            'attributes'               => esc_html__( 'Game Attributes', 'your-textdomain' ),
            'insert_into_item'         => esc_html__( 'Insert into game', 'your-textdomain' ),
            'uploaded_to_this_item'    => esc_html__( 'Uploaded to this game', 'your-textdomain' ),
            'featured_image'           => esc_html__( 'Featured image', 'your-textdomain' ),
            'set_featured_image'       => esc_html__( 'Set featured image', 'your-textdomain' ),
            'remove_featured_image'    => esc_html__( 'Remove featured image', 'your-textdomain' ),
            'use_featured_image'       => esc_html__( 'Use as featured image', 'your-textdomain' ),
            'menu_name'                => esc_html__( '게임', 'your-textdomain' ),
            'filter_items_list'        => esc_html__( 'Filter 게임 list', 'your-textdomain' ),
            'filter_by_date'           => esc_html__( '', 'your-textdomain' ),
            'items_list_navigation'    => esc_html__( '게임 list navigation', 'your-textdomain' ),
            'items_list'               => esc_html__( '게임 list', 'your-textdomain' ),
            'item_published'           => esc_html__( 'Game published.', 'your-textdomain' ),
            'item_published_privately' => esc_html__( 'Game published privately.', 'your-textdomain' ),
            'item_reverted_to_draft'   => esc_html__( 'Game reverted to draft.', 'your-textdomain' ),
            'item_scheduled'           => esc_html__( 'Game scheduled.', 'your-textdomain' ),
            'item_updated'             => esc_html__( 'Game updated.', 'your-textdomain' ),
        ];
        $args = [
            'label'               => esc_html__( '게임', 'your-textdomain' ),
            'labels'              => $labels,
            'description'         => '',
            'public'              => true,
            'hierarchical'        => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'show_in_rest'        => true,
            'query_var'           => true,
            'can_export'          => true,
            'delete_with_user'    => true,
            'has_archive'         => true,
            'rest_base'           => '',
            'show_in_menu'        => true,
            'menu_position'       => '',
            'menu_icon'           => 'dashicons-admin-generic',
            'capability_type'     => 'post',
            'supports'            => ['title', 'editor', 'thumbnail'],
            'taxonomies'          => [],
            'rewrite'             => [
                'with_front' => false,
            ],
        ];
    
        register_post_type( 'game', $args );
    }
}
