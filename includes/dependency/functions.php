<?php

function racing_find_email_by_password($password) {
  global $wpdb;

  $meta_key = AVS_Constant::METABOX_USER_PASSWORD;

  $query = $wpdb->prepare("
    SELECT u.user_email
    FROM {$wpdb->prefix}users AS u
    JOIN {$wpdb->prefix}usermeta AS um ON u.ID = um.user_id
    WHERE um.meta_key = '{$meta_key}' AND um.meta_value = %s
  ", $password);

  $email = $wpdb->get_var($query);

  return $email ? $email : false;
}

function racing_error_response($error_code = null, $error_message = '', $dev_error_message = '', $status_code = 400, $data = null) {
  $response = array(
    'code' => $error_code,
    'message' => $error_message,
    'dev_error_message' => $dev_error_message,
    'data' => $data
  );

  wp_send_json( $response, $status_code );
}

function racing_success_response($success_code = null, $success_message = '', $data = null, $status_code = 200) {
  $response = array(
    'code' => $success_code,
    'message' => $success_message,
    'data' => $data
  );
  wp_send_json( $response, $status_code );
}

function racing_get_user_email_by_id($user_id) {
  $user = get_userdata($user_id);

  if ($user) {
    return $user->user_email;
  }

  return null; // Yep, user doesn't exist or no email.
}

function racing_get_main_answer_of_team($team) {
  $answer = rwmb_meta(AVS_Constant::METABOX_QUIZ_MAIN_ANSWER, ['object_type' => 'user'], $team);
  return $answer;
}

// team = user_id
function racing_set_main_answer_to_team($team, $answer) {
  return rwmb_set_meta($team, AVS_Constant::METABOX_QUIZ_MAIN_ANSWER, $answer, ['object_type' => 'user'] );
}

// team = user_id
function racing_add_sub_answer_to_team($team, $answer) {
  global $wpdb;

  $table = "{$wpdb->prefix}usermeta";

  // Loop through the meta keys and check if a row exists for each one. If not, insert it.
  for ($i = 1; $i <= 20; $i++) {
      $meta_key = AVS_Constant::METABOX_QUIZ_SUB_ANSWER . "_{$i}";

      $exists = $wpdb->get_var($wpdb->prepare(
          "SELECT COUNT(*) FROM $table WHERE meta_key = %s AND user_id = %d", 
          $meta_key, 
          $team
      ));

      // If the row doesn't exist, insert it.
      if (!$exists) {
          $wpdb->insert(
              $table, 
              [
                  'user_id' => $team,
                  'meta_key' => $meta_key,
                  'meta_value' => $answer
              ]
          );
          break; // STOP
      }
  }
}

function racing_reset_sub_answer_of_team($team) {
  global $wpdb;

  $table = "{$wpdb->prefix}usermeta";

  // Loop through the meta keys and check if a row exists for each one. If not, insert it.
  for ($i = 1; $i <= 20; $i++) {
    $meta_key = AVS_Constant::METABOX_QUIZ_SUB_ANSWER . "_{$i}";

    // remove row where meta_key is $meta_key and user_id is $team
    $wpdb->delete($table, array('meta_key' => $meta_key, 'user_id' => $team));
  }
}

function racing_get_count_of_sub_answer($team) {
  $list = racing_get_all_sub_answer_list($team);
  $count = count($list);

  return $count;
}

function racing_get_all_sub_answer_list($team) {
  global $wpdb;
  
  $table = "{$wpdb->prefix}usermeta";
  $base_field_id = AVS_Constant::METABOX_QUIZ_SUB_ANSWER;

  // Create an array for field_ids from team_sub_answer_1 to team_sub_answer_20
  $field_ids = [];
  for ($i = 1; $i <= 20; $i++) {
    $field_ids[] = $base_field_id . '_' . $i;
  }

  // Create placeholders for each of the field_ids
  $placeholders = implode(', ', array_fill(0, count($field_ids), '%s'));

  // Prepare the SQL statement
  $sql = "SELECT `meta_value` FROM $table WHERE `meta_key` IN ($placeholders) AND user_id = '{$team}'";
  $query = $wpdb->prepare($sql, ...$field_ids);

  $results = $wpdb->get_col($query);

  // Filter out empty values
  $filtered_results = array_filter($results, function($value) {
    return !empty($value);
  });

  // Get the count of the filtered results
  return $filtered_results;
}

function racing_get_user_ids_except_admins() {
  $args = array(
    'role__not_in' => array('administrator'), // Sorry, administrators, not today!
    'fields' => 'ID'
  );
  
  $users = get_users($args);
  return $users;
}

function racing_get_user_display_name_by_id($user_id) {
  $user = get_userdata($user_id);
  
  if ($user) { // Check if the user exists.
    return $user->display_name;
  }
  
  return ''; // In case the user ID doesn't exist or there's an error.
}

function racing_get_max_team_member_count() {
  $user_ids = racing_get_user_ids_except_admins();
  $field_id = AVS_Constant::METABOX_TEAM_MEMBER_COUNT;
  $max_team_count = array_reduce($user_ids, function($carry, $team_id) use ($field_id) {
    $member_count = rwmb_meta($field_id, array('object_type' => 'user'), $team_id);
    if ($carry < $member_count) {
      return $member_count;
    }
    return $carry;
  }, 8);
  return $max_team_count;
}

function racing_get_team_member_count($team_id) {
  $member_count = rwmb_meta($field_id, array('object_type' => 'user'), $team_id);
  return empty($member_count) ? 10 : $member_count;
}

function racing_strip_all_space($str) {
  return preg_replace('/\s+/', '', $str);
}

function racing_get_logo() {
  $logo = rwmb_meta( AVS_Constant::METABOX_LOGO, ['object_type' => 'setting'],  AVS_Constant::METABOX_GAME_SETTING_PAGE );
  if (isset($logo['full_url'])) {
    return $logo['full_url'];
  }
  return '';
}

function racing_get_game_status() {
  $game_status = rwmb_meta( AVS_Constant::METABOX_GAME_STATUS, ['object_type' => 'setting'],  AVS_Constant::METABOX_GAME_SETTING_PAGE );
  return $game_status;
}

function racing_set_game_status($game_status) {
  if ($game_status !== 'reset' && $game_status !== 'ready' && $game_status !== 'start') {
    return;
  }

  rwmb_set_meta(AVS_Constant::METABOX_GAME_SETTING_PAGE, AVS_Constant::METABOX_GAME_STATUS, $game_status, ['object_type' => 'setting']);
}

function racing_get_team_count() {
  $team_count = rwmb_meta( AVS_Constant::METABOX_TEAM_COUNT, ['object_type' => 'setting'],  AVS_Constant::METABOX_GAME_SETTING_PAGE );
  return $team_count;
}

function racing_get_team_word_point($team_id) {
  global $wpdb;

  // get word_point from usermeta where usermeta.key is AVS_Constant::METABOX_TEAM_WORD_POINT ans user_id is $team_id
  $query = $wpdb->prepare("
      SELECT meta_value
      FROM {$wpdb->usermeta} AS usermeta
      WHERE usermeta.meta_key = %s
      AND usermeta.user_id = %s
  ", AVS_Constant::METABOX_TEAM_WORD_POINT, $team_id);

  $result = $wpdb->get_col($query);

  return count($result) === 0 ? 0 : intval($result[0]);
}

function racing_get_speed_per_word() {
  $speed_per_word = rwmb_meta( AVS_Constant::METABOX_SPEED_PER_WORD, ['object_type' => 'setting'],  AVS_Constant::METABOX_GAME_SETTING_PAGE );
  return intval($speed_per_word);
}

function racing_reset_team_word_point($team_id) {
  global $wpdb;

  $query = $wpdb->prepare("
    UPDATE {$wpdb->usermeta}
    SET meta_value = %s
    WHERE user_id = %s
    AND meta_key = %s
  ", 0, $team_id, AVS_Constant::METABOX_TEAM_WORD_POINT);

  $wpdb->query($query);
}

function racing_reset_all_team_word_point() {
  $user_ids = racing_get_user_ids_except_admins();
  foreach ($user_ids as $user_id) {
    racing_reset_team_word_point($user_id);
  }
}

function racing_get_user_id_by_team($team_number) {
  global $wpdb;  // WordPress database 접근을 위한 글로벌 변수

  // SQL 쿼리를 준비합니다.
  $query = $wpdb->prepare("
      SELECT user_id
      FROM {$wpdb->users} AS users
      JOIN {$wpdb->usermeta} AS usermeta ON users.ID = usermeta.user_id
      WHERE usermeta.meta_key = 'team'
      AND usermeta.meta_value = %s
  ", 'team_' . $team_number);

  // SQL 쿼리를 실행하고 결과를 반환합니다.
  $result = $wpdb->get_col($query);

  return $result[0];
}

function racing_get_all_team_word_point_list() {
  $team_number_list = AVS_Constant::TEAM_NUMBER_LIST;
  // please use array_reduce
  $team_id_list = array_reduce($team_number_list, function($carry, $team_number) use ($self) {
    $user_ids = racing_get_user_id_by_team($team_number);
    $carry[] = $user_ids;
    return $carry;
  }, []);

  // index를 팀으로 사용해도 되것지?
  $team_word_point_list = array_map(function($team_id) use ($self) {
    $word_point = racing_get_team_word_point($team_id);
    return $word_point;
  }, $team_id_list);

  return $team_word_point_list;
}
