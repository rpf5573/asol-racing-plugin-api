<?php

class AVS_Meta_Boxes {
  public function __construct(AVS_Loader $loader) {
    $this->loader = $loader;

    // 우선순위를 낮춰야 한다
    $this->loader->add_action('profile_update', $this, 'update_custom_field_on_password_change', 100, 2);
    $this->loader->add_filter('mb_settings_pages', $this, 'word_input_page');
    $this->loader->add_filter('mb_settings_pages', $this, 'game_operation_page');
    
    // $this->loader->add_filter( 'rwmb_meta_boxes', $this, 'custom_field_logo' );
    $this->loader->add_filter( 'rwmb_meta_boxes', $this, 'word_input_fields_at_setting_page' );
    $this->loader->add_filter( 'rwmb_meta_boxes', $this, 'game_setting_fields_at_setting_page' );

    $this->loader->add_action('mb_settings_page_load', $this, 'game_status_reset_to_ready', 1000);
    $this->loader->add_filter('rwmb_meta_boxes', $this, 'user_meta');
  }

  // Function to update custom field when a user's password is updated
  public function update_custom_field_on_password_change($user_id, $old_user_data) {
    if (!isset($_POST['pass1']) || empty($_POST['pass1'])) {
      return;
    }
    $user = get_userdata($user_id);
    $new_pw = $_POST['pass1'];

    rwmb_set_meta( $user_id, AVS_Constant::METABOX_USER_PASSWORD, $new_pw, [ 'object_type' => 'user' ] );
  }

  public function word_input_page( $settings_pages ) {
    $settings_pages[] = [
      'menu_title' => '단어 입력',
      'id'         => AVS_Constant::METABOX_SETTING_PAGE_WORD_INPUT,
      'position'   => 26,
      'style'      => 'no-boxes',
      'columns'    => 1,
      'icon_url'   => 'dashicons-admin-settings',
    ];

    return $settings_pages;
  }

  public function game_operation_page( $settings_pages ) {
    $settings_pages[] = [
      'menu_title' => '게임 설정',
      'id'         => AVS_Constant::METABOX_GAME_SETTING_PAGE,
      'position'   => 26,
      'style'      => 'no-boxes',
      'columns'    => 1,
      'icon_url'   => 'dashicons-admin-settings',
    ];

    return $settings_pages;
  }

  public function word_input_fields_at_setting_page( $meta_boxes ) {
      $meta_boxes[] = [
          'title'          => __( '단어 설정' ),
          'id'             => 'word_input_field_group',
          'settings_pages' => ['word_input_setting_page'],
          'fields'         => [
              [
                  'name'              => __( 'Word List' ),
                  'id'                => AVS_Constant::METABOX_WORD_LIST,
                  'type'              => 'group',
                  'collapsible'       => true,
                  'group_title'       => 'item',
                  'clone'             => true,
                  'clone_default'     => true,
                  'clone_as_multiple' => true,
                  'columns'           => 6,
                  'fields'            => [
                      [
                          'name'    => __( 'Required' ),
                          'id'      => 'is_this_item_required',
                          'type'    => 'checkbox',
                          'std'     => true,
                          'columns' => 2,
                      ],
                      [
                          'name'    => __( 'Word (=Answer)' ),
                          'id'      => 'word',
                          'type'    => 'text',
                          'columns' => 10,
                      ],
                  ],
              ],
              [
                  'name'              => __( 'Image List' ),
                  'id'                => AVS_Constant::METABOX_IMAGE_LIST,
                  'type'              => 'group',
                  'collapsible'       => true,
                  'group_title'       => 'Item',
                  'clone'             => true,
                  'clone_default'     => true,
                  'clone_as_multiple' => true,
                  'columns'           => 6,
                  'fields'            => [
                      [
                          'name'    => __( 'Required' ),
                          'id'      => 'is_this_item_required',
                          'type'    => 'checkbox',
                          'std'     => true,
                          'columns' => 2,
                      ],
                      [
                        'name'    => __( 'Answer' ),
                        'id'      => 'image_item_answer',
                        'type'    => 'text',
                        'required' => true,
                        'columns' => 4,
                      ],
                      [
                          'name'    => __( 'Image' ),
                          'id'      => 'image_item',
                          'type'    => 'single_image',
                          'required' => true,
                          'columns' => 6,
                      ],
                  ],
              ],
          ],
      ];

      return $meta_boxes;
  }

  public function game_setting_fields_at_setting_page( $meta_boxes ) {
    $meta_boxes[] = [
        'title'          => __( '게임 설정 필드' ),
        'id'             => '%ea%b2%8c%ec%9e%84-%ec%84%a4%ec%a0%95-%ed%95%84%eb%93%9c',
        'settings_pages' => [AVS_Constant::METABOX_GAME_SETTING_PAGE],
        'fields'         => [
            [
                'name'              => __( '팀수' ),
                'id'                => AVS_Constant::METABOX_TEAM_COUNT,
                'label_description' => __( '전체 팀수를 입력해주세요' ),
                'min'               => 2,
                'max'               => 20,
                'step'              => 1,
                'std'               => 5,
                'required'          => true,
                'columns'           => 3,
            ],
            [
              'name'              => __( 'Game Status' ),
              'id'                => AVS_Constant::METABOX_GAME_STATUS,
              'type'              => 'radio',
              'label_description' => __( '현재 경기가 어떤 상태인지 선택해주세요' ),
              'options'           => [
                  'ready' => __( '준비중' ),
                  'reset' => __( '리셋' ),
                  'start' => __( '시작' ),
              ],
              'std'               => 'ready',
              'required'          => true,
              'class' => 'game-status-field',
            ],
            [
              'name'              => __( '말 속도' ),
              'id'                => AVS_Constant::METABOX_SPEED_PER_WORD,
              'type'              => 'radio',
                'label_description' => __( '단어 1개를 맞추었을때 부스터 속도' ),
                'options'           => [
                    5  => __( '1단계 (5)', 'your-text-domain' ),
                    10 => __( '2단계 (10)', 'your-text-domain' ),
                    15 => __( '3단계 (15)', 'your-text-domain' ),
                    20 => __( '4단계 (20)', 'your-text-domain' ),
                ],
                'std'               => 10,
                'columns'           => 4,
                'required'          => true,
            ],
            [
              'name'              => __( '총 경기장 길이' ),
              'id'                => AVS_Constant::METABOX_RAIL_DISTANSE,
              'type'              => 'number',
              'label_description' => __( '경기장의 총 길이입니다' ),
              'min'               => 100,
              'max'               => 10000,
              'step'              => 1,
              'std'               => 1000,
              'required'          => true,
              'columns'           => 3,
            ],
        ],
    ];

    return $meta_boxes;
  }

  public function game_status_reset_to_ready() {
    $key = AVS_Constant::METABOX_GAME_STATUS;
    if (isset($_POST[$key])) {
      $status = $_POST[$key];
      if ($status === 'reset') {
        racing_set_game_status('ready');
      } else if ($status === 'start') {
        racing_set_game_status('running');
      }
    }
  }

  public function user_meta( $meta_boxes ) {
    $meta_boxes[] = [
        'title'  => __( '유저 메타 데이타' ),
        'id'     => 'user_meta_field_group_id',
        'type'   => 'user',
        'fields' => [
            [
                'name'     => __( 'Word Point' ),
                'id'       => AVS_Constant::METABOX_TEAM_WORD_POINT,
                'type'     => 'number',
                'readonly' => false,
                'default'  => 0,
            ],
            [
                'name'              => __( 'Team' ),
                'id'                => AVS_Constant::METABOX_USER_TEAM,
                'type'              => 'select',
                'label_description' => __( '현재 유저의 팀을 설정해 주세요' ),
                'options'           => [
                    'team_1'  => __( '1팀' ),
                    'team_2'  => __( '2팀' ),
                    'team_3'  => __( '3팀' ),
                    'team_4'  => __( '4팀' ),
                    'team_5'  => __( '5팀' ),
                    'team_6'  => __( '6팀' ),
                    'team_7'  => __( '7팀' ),
                    'team_8'  => __( '8팀' ),
                    'team_9'  => __( '9팀' ),
                    'team_10' => __( '10팀' ),
                    'team_11' => __( '11팀' ),
                    'team_12' => __( '12팀' ),
                ],
            ],
            [
              'name'              => __( '팀비밀번호' ),
              'id'                => AVS_Constant::METABOX_USER_PASSWORD,
              'type'              => 'text',
              'label_description' => __( '현재 팀의 비밀번호입니다' ),
              'readonly'          => true,
            ],
        ],
    ];

    return $meta_boxes;
  }
}
