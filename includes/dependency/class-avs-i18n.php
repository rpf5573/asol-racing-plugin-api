<?php

class AVS_i18n
{
  /**
   * The domain specified for this plugin.
   *
   * @since    1.0.0
   *
   * @var string The domain identifier for this plugin.
   */
  private string $domain;

  public function load_plugin_textdomain()
  {
    load_plugin_textdomain(
      $this->domain,
      false,
      dirname(plugin_basename(__FILE__), 2) . '/languages/'
    );
  }

  public function set_domain(string $domain)
  {
    $this->domain = $domain;
  }
}
