<?php

class AVS_Constant {
  const METABOX_USER_PASSWORD = 'team_password';
  const METABOX_SETTING_PAGE_WORD_INPUT = 'word_input_setting_page';
  const METABOX_GAME_SETTING_PAGE = 'game_setting_page';
  const METABOX_TEAM_COUNT = 'team_count';
  const METABOX_QUIZ_MAIN_ANSWER = 'main_team_answer';
  const METABOX_TEAM_MEMBER_COUNT = 'team_member_count';
  const METABOX_TEAM_TOTAL_POINT = 'team_total_point';
  const METABOX_LOGO = 'logo';
  const METABOX_GAME_STATUS = 'game_status';
  const METABOX_TEAM_WORD_POINT = 'word_point';
  const METABOX_USER_TEAM = 'team';
  const METABOX_TOTAL_WORD_COUNT = 'total_word_count';
  const METABOX_WORD_LIST = 'word_list';
  const METABOX_IMAGE_LIST = 'image_list';
  const TEAM_NUMBER_LIST = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const METABOX_SPEED_PER_WORD = 'speed_per_word';
  const METABOX_RAIL_DISTANSE = 'rail_distanse';
}
