<?php

/**
 * The file that defines the core plugin class.
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://enriquechavez.co
 * @since      1.0.0
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 *
 * @author     Enrique Chavez <noone@tmeister.net>
 */
class AVS_Init
{
  /**
   * The loader that's responsible for maintaining and registering all hooks that power
   * the plugin.
   *
   * @since    1.0.0
   *
   * @var AVS_Loader Maintains and registers all hooks for the plugin.
   */
  protected AVS_Loader $loader;

  /**
   * The unique identifier of this plugin.
   *
   * @since    1.0.0
   *
   * @var string The string used to uniquely identify this plugin.
   */
  protected string $plugin_name;

  /**
   * The current version of the plugin.
   *
   * @since    1.0.0
   *
   * @var string The current version of the plugin.
   */
  protected string $version;

  /**
   * Define the core functionality of the plugin.
   *
   * Set the plugin name and the plugin version that can be used throughout the plugin.
   * Load the dependencies, define the locale, and set the hooks for the admin area and
   * the public-facing side of the site.
   *
   * @since    1.0.0
   */
  public function __construct()
  {

    $this->plugin_name = 'racing';
    $this->version = '1';

    $this->load_dependencies();
    $this->set_locale();
    $this->load_routers();
  }

  /**
   * Load the required dependencies for this plugin.
   *
   * Include the following files that make up the plugin:
   *
   * - AVS_Loader. Orchestrates the hooks of the plugin.
   * - AVS_i18n. Defines internationalization functionality.
   * - AVS_Admin. Defines all hooks for the admin area.
   * - AVS_Public. Defines all hooks for the public side of the site.
   *
   * Create an instance of the loader which will be used to register the hooks
   * with WordPress.
   *
   * @since    1.0.0
   */
  private function load_dependencies()
  {
    /**
     * Load dependencies managed by composer.
     */
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/vendor/autoload.php';

    /**
     * The class responsible for orchestrating the actions and filters of the
     * core plugin.
     */
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/dependency/class-avs-loader.php';

    /**
     * The class responsible for defining internationalization functionality
     * of the plugin.
     */
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/dependency/class-avs-i18n.php';

    /**
     * Class responsible for creating a `wrapper namespace` to load the Firebase's JWT & Key
     * classes and prevent conflicts with other plugins using the same library
     * with different versions.
     */
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/dependency/class-avs-namespace-wrapper.php';
    
    $this->loader = new AVS_Loader();

    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/dependency/class-avs-meta-boxes.php';
    new AVS_Meta_Boxes($this->loader);

    // require_once plugin_dir_path(dirname(__FILE__)) . 'includes/dependency/class-avs-post-type.php';
    // new AVS_Post_Type($this->loader);
  }

  private function set_locale()
  {
    $plugin_i18n = new AVS_i18n();
    $plugin_i18n->set_domain($this->get_plugin_name());
    $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
  }

  private function load_routers()
  {
    $namespace = $this->plugin_name . '/v' . intval($this->version);

    // Login
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/rest-api/login/class-avs-rest-login.php';
    new AVS_Rest_Login($namespace, $this->loader);

    // Logo
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/rest-api/logo/class-avs-rest-get-logo.php';
    new AVS_Rest_Get_Logo($namespace, $this->loader);

    // Word
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/rest-api/word/class-avs-rest-get-all-team-word-point.php';
    new AVS_Rest_Get_All_Team_Word_Point($namespace, $this->loader);
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/rest-api/word/class-avs-rest-increase-word-point.php';
    new AVS_Rest_Increase_Word_Point($namespace, $this->loader);
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/rest-api/word/class-avs-rest-get-all-words-and-images.php';
    new AVS_Rest_Get_All_Words_And_Images($namespace, $this->loader);

    // Game Setting
    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/rest-api/game-settings/class-avs-rest-game-settings.php';
    new AVS_Rest_Game_Settings($namespace, $this->loader);

    // require_once plugin_dir_path(dirname(__FILE__)) . 'includes/rest-api/word/class-avs-rest-post-word.php';
    // new AVS_Rest_Post_Word($namespace, $this->loader);
  }

  public function run()
  {
    $this->loader->run();
  }

  public function get_plugin_name(): string
  {
    return $this->plugin_name;
  }

  public function get_loader(): AVS_Loader
  {
    return $this->loader;
  }

  public function get_version(): string
  {
    return $this->version;
  }
}
