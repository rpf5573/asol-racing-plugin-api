<?php

class AVS_Rest_Get_Logo {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/logo'; // signup은 namespace가 없다

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route($this->namespace, 'get-logo-url', [
      'methods'             => 'GET',
      'callback'            => function(WP_REST_Request $request){
        $logo_url = racing_get_logo();
        return racing_success_response( 'success_get_logo_url', '성공적으로 로고를 불러왔습니다', array(
          'logo_url' => $logo_url
        ) );
      },
      'permission_callback' => function() {
        return true;
      },
    ]);
  }
}
