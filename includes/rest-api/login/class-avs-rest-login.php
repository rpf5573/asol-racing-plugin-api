<?php

use Racing\Firebase\JWT\JWT;
use Racing\Firebase\JWT\Key;

class AVS_Rest_Login {
  private string $namespace;

  private ?WP_Error $jwt_error = null;

  protected AVS_Loader $loader;

  private array $supported_algorithms = ['HS256', 'HS384', 'HS512', 'RS256', 'RS384', 'RS512', 'ES256', 'ES384', 'ES512', 'PS256', 'PS384', 'PS512'];

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace; // login은 namespace가 없다

    // 아래 3개는 auth로 옮겨야 겠다
    // add_action( 'rest_api_init', array($this, 'allow_all_cors'));
    $this->loader->add_action('rest_api_init', $this, 'allow_all_cors', 0);
    $this->loader->add_filter('rest_pre_dispatch', $this, 'rest_pre_dispatch', 1, 2);
    $this->loader->add_filter('determine_current_user', $this, 'determine_current_user');

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes()
  {
    register_rest_route($this->namespace, 'login', [
      'methods'             => 'POST',
      'callback'            => function(WP_REST_Request $request){
        $secret_key = defined('AVS_JWT_AUTH_SECRET_KEY') ? AVS_JWT_AUTH_SECRET_KEY : false;
        if (!$secret_key) {
          return racing_error_response( 'jwt_auth_bad_config', 'JWT가 제대로 설정되지 않았습니다. 관리자에게 문의해주세요.', '', 500 );
        }

        $password   = $request->get_param('password');
        $email = racing_find_email_by_password($password);

        function_exists('ray') && ray('password - email', [$password, $email]);

        if (empty($email)) {
          return racing_error_response( 'incorrect_password', '비밀번호를 다시 확인해주세요', '', 500 );
        }

        $user = wp_authenticate($email, $password);
        if (is_wp_error($user)) {
          $error_code = $user->get_error_code();
          $error_message = '';
    
          switch ($error_code) {
            case 'invalid_email':
                $error_message = "존재하지 않는 이메일 입니다";
                break;
            case 'invalid_username':
                $error_message = "존재하지 않는 이메일 입니다";
                break;
            case 'incorrect_password':
                $error_message = "입력한 사용자 이름에 대한 비밀번호가 잘못되었습니다.";
                break;
            default:
                $error_message = "알 수 없는 오류가 발생했습니다: " . $error_code;
                break;
          }

          return racing_error_response( $error_code, $error_message, '', 401 );
        }

        $issuedAt  = time();
        $notBefore = $issuedAt;
        $expire    = $issuedAt + (DAY_IN_SECONDS * 7);

        $token = [
          'iss'  => get_bloginfo('url'),
          'iat'  => $issuedAt,
          'nbf'  => $notBefore,
          'exp'  => $expire,
          'data' => [
            'user' => [
              'id' => $user->data->ID,
            ],
          ],
        ];

        $algorithm = $this->get_algorithm();
        if ($algorithm === false) {
          return racing_error_response( 'jwt_auth_unsupported_algorithm', 'JWT 알고리즘이 존재하지 않습니다. 관리자에게 문의해주세요.', '', 403 );
        }

        $token = JWT::encode(
          $token,
          $secret_key,
          $algorithm
        );

        $data = [
          'token'             => $token,
          'user_email'        => $user->data->user_email,
          'user_nicename'     => $user->data->user_nicename,
          'user_display_name' => $user->data->display_name,
          'user_id'           => $user->data->ID,
        ];

        $user_id = $user->data->ID;

        return racing_success_response( 'login_success', '성공적으로 로그인 되었습니다', $data );
      },
      'args' => [
        'password' => [
          'required' => true,
          'description' => 'Password',
          'validate_callback' => function($param, $request, $key) {
            if (!isset($param) || empty($param)) {
              return new WP_Error( 'empty_password', '비밀번호를 입력해주세요' );
            }
            return true;
          },
        ],
      ],
      'permission_callback' => '__return_true',
    ]);
  }

  private function get_algorithm()
  {
    $algorithm = 'HS256';
    if (!in_array($algorithm, $this->supported_algorithms)) {
      return false;
    }

    return $algorithm;
  }

  public function rest_pre_dispatch($request)
  {
    if (is_wp_error($this->jwt_error)) {
      return $this->jwt_error;
    }

    return $request;
  }

  // CORS 헤더를 추가하는 함수
  function allow_all_cors() {
    remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
    add_filter( 'rest_pre_serve_request', function () {
      header( "Access-Control-Allow-Origin: *" );
      header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
      header( 'Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization' );
      header( 'Access-Control-Allow-Credentials: true' );
    } );
  }

  public function determine_current_user($user)
  {
    $rest_api_slug = rest_get_url_prefix();
    $requested_url = sanitize_url($_SERVER['REQUEST_URI']);

    if (strpos($requested_url, $rest_api_slug) === false || $user) {
      return $user;
    }

    $validate_uri = strpos($requested_url, 'token/validate');
    if ($validate_uri > 0) {
      return $user;
    }

    $auth_header = isset($_SERVER['HTTP_AUTHORIZATION']) ? sanitize_text_field($_SERVER['HTTP_AUTHORIZATION']) : false;
    /* Double check for different auth header string (server dependent) */
    if (!$auth_header) {
      $auth_header = isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) ? sanitize_text_field($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) : false;
    }

    if (!$auth_header) {
      return $user;
    }

    $token = $this->validate_token(new WP_REST_Request(), $auth_header);

    if (is_wp_error($token)) {
      if ($token->get_error_code() != 'jwt_auth_no_auth_header') {
        /** If there is an error, store it to show it after see rest_pre_dispatch */
        $this->jwt_error = $token;
      }

      return $user;
    }

    $user_id = $token->data->user->id;

    return $user_id;
  }

  public function validate_token(WP_REST_Request $request, $custom_token = false)
  {
    /*
		 * Looking for the Authorization header
		 *
		 * There is two ways to get the authorization token
		 *  1. via WP_REST_Request
		 *  2. via custom_token, we get this for all the other API requests
		 *
		 * The get_header( 'Authorization' ) checks for the header in the following order:
		 * 1. HTTP_AUTHORIZATION
		 * 2. REDIRECT_HTTP_AUTHORIZATION
		 *
		 * @see https://core.trac.wordpress.org/ticket/47077
		 */

    $auth_header = $custom_token ?: $request->get_header('Authorization');

    if (!$auth_header) {
      return racing_error_response( 'jwt_auth_no_auth_header', '인증 헤더를 찾을 수 없습니다', '', 403 );
    }

    /*
		 * Extract the authorization header
		 */
    [$token] = sscanf($auth_header, 'Bearer %s');

    /**
     * if the format is not valid return an error.
     */
    if (!$token) {
      return racing_error_response( 'jwt_auth_bad_auth_header', '인증 헤더가 잘못되었습니다', '', 403 );
    }

    /** Get the Secret Key */
    $secret_key = defined('AVS_JWT_AUTH_SECRET_KEY') ? AVS_JWT_AUTH_SECRET_KEY : false;
    if (!$secret_key) {
      return racing_error_response( 'jwt_auth_bad_config', 'JWT가 제대로 설정되어있지 않습니다. 관리자에게 문의해주시기 바랍니다', '', 403 );
    }

    /** Try to decode the token */
    try {
      $algorithm = $this->get_algorithm();
      if ($algorithm === false) {
        return racing_error_response( 'jwt_auth_unsupported_algorithm', '지원하지 않는 JWT 알고리즘입니다', '', 403 );
      }

      $token = JWT::decode($token, new Key($secret_key, $algorithm));

      /** The Token is decoded now validate the iss */
      if ($token->iss !== get_bloginfo('url')) {
        return racing_error_response( 'jwt_auth_bad_iss', '토큰이 잘못되었습니다', '', 403 );
      }

      /** So far so good, validate the user id in the token */
      if (!isset($token->data->user->id)) {
        return racing_error_response( 'jwt_auth_bad_request', '토큰에 유저 아이디가 없습니다', '', 403 );
      }

      /** Everything looks good return the decoded token if we are using the custom_token */
      if ($custom_token) {
        return $token;
      }

      return racing_success_response( 'jwt_auth_valid_token', '토큰이 유효합니다', '', 200 );
    } catch (Exception $e) {
      return racing_error_response( 'jwt_auth_invalid_token', '토큰이 잘못되었습니다', '', 403 );
    }
  }
}
