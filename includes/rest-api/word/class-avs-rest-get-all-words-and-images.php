<?php

class AVS_Rest_Get_All_Words_And_Images {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/word';

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route($this->namespace, 'get-all-words-and-images', [
      'methods'             => 'GET',
      'callback'            => function(WP_REST_Request $request){
        $words = $this->get_all_words();
        $images = $this->get_all_images();
        $all_words_and_images = $this->merge_and_reformat_and_shuffle($words, $images);

        return racing_success_response( 'success_get_all_words_and_images', '성공적으로 모든 단어와 이미지들을 가져왔습니다', array(
          'items' => $all_words_and_images
        ) );
      },
      'permission_callback' => function() {
        return true;
      },
    ]);
  }

  public function get_all_words() {
    $field_id = AVS_Constant::METABOX_WORD_LIST;
    $setting_page_id = AVS_Constant::METABOX_SETTING_PAGE_WORD_INPUT;
    $args = ['object_type' => 'setting'];
    $words = rwmb_meta( $field_id, $args, $setting_page_id );
    return $words ?? [];
  }

  public function get_all_images() {
    $field_id = AVS_Constant::METABOX_IMAGE_LIST;
    $setting_page_id = AVS_Constant::METABOX_SETTING_PAGE_WORD_INPUT;
    $args = ['object_type' => 'setting'];
    $images = rwmb_meta( $field_id, $args, $setting_page_id );
    return $images ?? [];
  }

  private function merge_and_reformat_and_shuffle($words, $images) {
    $merged = [];

    foreach ($words as $group) {
      $word = $group['word'];
      $merged[] = ['type' => 'text', 'value' => $word, 'answer' => $word];
    }

    foreach ($images as $group) {
      $image_id = $group['image_item'];
      $image_url = wp_get_attachment_image_url($image_id, 'full');
      $answer = $group['image_item_answer'];
      $merged[] = ['type' => 'img', 'value' => $image_url, 'answer' => $answer]; 
    }

    shuffle($merged);

    return $merged;
  }
}
