<?php

class AVS_Rest_Increase_Word_Point {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/word';

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route( $this->namespace, 'increase-word-point', [
      'methods'       => 'POST',
      'callback'      => function(WP_REST_Request $request) {
        $game_status = racing_get_game_status();
        if ($game_status !== 'start') {
          return racing_error_response('error_game_not_started', '게임이 시작되지 않았습니다', array());
        }

        $team_id = $request->get_param('team_id');
        $speed_per_word = racing_get_speed_per_word($team_id);
        $team_word_point = racing_get_team_word_point($team_id);
        $team_word_point += $speed_per_word;

        $this->update_team_word_point($team_id, $team_word_point);

        return racing_success_response('success_update_word_point', '성공적으로 팀의 단어 점수를 업데이트 했습니다', array());
      },
      'args'          => array(
        'team_id' => array(
          'required' => true,
          'type' => 'string',
          'description' => '팀',
        ),
      ),
    ]);
  }

  public function update_team_word_point($team_id, $point) {
    rwmb_set_meta($team_id, AVS_Constant::METABOX_TEAM_WORD_POINT, $point, ['object_type' => 'user']);
  }
}
