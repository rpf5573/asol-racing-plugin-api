<?php

class AVS_Rest_Get_All_Team_Word_Point {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/word';

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route($this->namespace, 'get-all-team-word-point', [
      'methods'             => 'GET',
      'callback'            => function(WP_REST_Request $request){
        $all_team_word_point_list = $this->get_all_team_word_point_list();

        return racing_success_response( 'success_get_all_team_word_point', '성공적으로 모든 팀의 현재 맞춘 단어의 갯수를 가져왔습니다', array(
          'all_team_word_point_list' => $all_team_word_point_list
        ) );
      },
      'permission_callback' => function() {
        return true;
      },
    ]);
  }

  public function get_all_team_word_point_list() {
    return racing_get_all_team_word_point_list();
  }

  public function get_team_word_point($team_id) {
    return racing_get_team_word_point($team_id);
  }
}
