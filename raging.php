<?php

/**
 * The plugin bootstrap file.
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 *
 * @wordpress-plugin
 * Plugin Name:       Racing
 * Description:       Create a rest-api and admin view for racing
 * Version:           1.0.0
 * Author:            airman5573
 * Author URI:        https://github.com/airman5573
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
  die;
}

define('AVS_JWT_AUTH_SECRET_KEY', 'racing-auth-jwt-secret-key');
define('JWT_AUTH_CORS_ENABLE', true);
define('GONGAM_GAME_PLUGIN_DIR_URL', plugin_dir_url(__FILE__));

require plugin_dir_path(__FILE__) . 'includes/dependency/class-avs-constant.php';
require plugin_dir_path(__FILE__) . 'includes/dependency/functions.php';

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-avs-init.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_avs(): void
{
  $plugin = new AVS_Init();
  $plugin->run();
}

run_avs();
